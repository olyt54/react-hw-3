import React, {useEffect, useState} from 'react';
import GoodsList from "../components/GoodsList/GoodsList";

const Favorites = (props) => {
    const {goods} = props;

    const [favorites, setFavorites] = useState(goods)

    const filterFavorites = () => {
        if (!localStorage.getItem("favorites")) {
            localStorage.setItem("favorites", JSON.stringify([]))
        }

        const favoritesCodes = JSON.parse(localStorage.getItem("favorites"));
        const newFavorites = goods.filter(item => favoritesCodes.includes(item.vendorCode));
        setFavorites(newFavorites);
    }

    useEffect(() => {
        filterFavorites()
    }, [goods]);

    const removeFromFavorites = (vendorCode) => {
        const newFavorites = favorites.filter(i => i.vendorCode !== vendorCode);

        setFavorites(newFavorites)
    }

    return (
        <>
            <GoodsList goods={favorites} updateFavorites={removeFromFavorites}/>
        </>
    );
};

export default Favorites;