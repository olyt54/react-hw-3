import React, {useEffect, useState} from 'react';
import GoodsList from "../components/GoodsList/GoodsList";

const Cart = (props) => {
    const {goods, isInCart} = props;

    const [cart, setCart] = useState(goods);

    const filterCart = () => {
        if (!localStorage.getItem("cart")) {
            localStorage.setItem("cart", JSON.stringify([]))
        }

        const favoritesCodes = JSON.parse(localStorage.getItem("cart"))
        const newCart = goods.filter(item => favoritesCodes.includes(item.vendorCode))
        setCart(newCart);
    }

    useEffect(() => {
       filterCart()
    }, [goods]);

    const removeFromCart = (vendorCode) => {
        const newCart = cart.filter(i => i.vendorCode !== vendorCode);

        setCart(newCart)
    }

    return (
        <>
            <GoodsList goods={cart} isInCart={isInCart} updateCart={removeFromCart}/>
        </>
    );
};

export default Cart;