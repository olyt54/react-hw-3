import React, {useState, useEffect} from 'react';
import './App.scss';
import axios from "axios";
import Header from "./components/Header/Header";
import AppRoutes from "./routes/AppRoutes";

const App = () => {
    const [goods, setGoods] = useState([]);

    const getGoods = () => {
        axios("/goods.json")
            .then(res => setGoods(res.data))
    };

    useEffect(() => {
        getGoods();
    }, [])

    return (
        <div className="App">
            <Header/>
            <div className="content-container">
                <AppRoutes goods={goods}/>
            </div>
        </div>
    )
}


export default App;
