import React from 'react';
import {Switch, Route} from "react-router-dom";
import GoodsList from "../components/GoodsList/GoodsList";
import Favorites from "../pages/Favorites";
import Cart from "../pages/Cart";


const AppRoutes = (props) => {
    const {goods} = props;

    return (
        <Switch>
            {console.log(goods)}
            <Route exact path="/" render={() => <GoodsList goods={goods}/>} />
            <Route exact path="/cart" render={() => <Cart goods={goods} isInCart={true}/>}/>
            <Route exact path="/favorites" render={() => <Favorites goods={goods}/>}/>
        </Switch>
    );
};

export default AppRoutes;