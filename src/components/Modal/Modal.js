import React from 'react';
import Button from "../Button/Button";
import propTypes from "prop-types"
import "./modal.scss"
import CloseIcon from "../CloseIcon/CloseIcon";

const Modal = (props) => {
    let closeBtn;
    if (props.closeButton) {
        closeBtn = <Button text={<CloseIcon color="white"/>} bgColor="transparent" handleBtn={props.handleCloseButton}/>
    }

    return (
        <div className="modal-bg" onClick={(event) => props.handleCloseOuter(event)}>
            <div className="modal">
                <div className="modal-head">
                    <h3 className="modal-head-text">{props.header}</h3>
                    {closeBtn}
                </div>
                <div className="modal-body">
                    <p className="modal-body-text">{props.text}</p>
                    <div className="modal-actions">
                        {props.actions.firstBtn}
                        {props.actions.secondBtn}
                    </div>
                </div>
            </div>
        </div>
        );
}

Modal.propTypes = {
    closeButton: propTypes.bool,
    handleClose: propTypes.func,
    header: propTypes.string,
    text: propTypes.string,
    actions: propTypes.object
}

export default Modal;

