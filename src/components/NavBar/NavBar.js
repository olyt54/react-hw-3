import React from 'react';
import {NavLink} from "react-router-dom";
import "./NavBar.scss"

const NavBar = () => {
    return (
        <nav className="navbar">
            <ul className="navbar__list">
                <li className="navbar__item"><NavLink exact
                             to="/"
                             activeStyle={{color: "#f9af72"}}
                             className="navbar__link"
                    >Home</NavLink>
                </li>
                <li className="navbar__item"><NavLink exact
                             to="/cart"
                             activeStyle={{color: "#f9af72"}}
                             className="navbar__link"
                    >Cart</NavLink>
                </li>
                <li className="navbar__item"><NavLink exact
                             to="/favorites"
                             activeStyle={{color: "#f9af72"}}
                             className="navbar__link"
                    >Favorites</NavLink>
                </li>
            </ul>
        </nav>
    );
};

export default NavBar;