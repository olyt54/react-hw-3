import React from 'react';
import GoodsItem from "../GoodsItem/GoodsItem";
import propTypes from "prop-types"

const GoodsList = (props) => {
    const {goods, isInCart, updateFavorites, updateCart} = props

    const goodsItems = goods.map(item => {
        return <GoodsItem {...item}
                          updateFavorites={updateFavorites}
                          updateCart={updateCart}
                          isInCart={isInCart}
                          key={item.vendorCode}
        />
    })

    return (
        <>
            {goodsItems}
            </>
    );
}

GoodsList.propTypes = {
    goods: propTypes.array,
    handleBtn: propTypes.func
}

export default GoodsList;